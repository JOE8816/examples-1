# This is the README.md file. 

## Explanation

In Git 'README.md' files are the default readme file names. This means that in advanced GUI (Graphical User Interfaces) such as the web interfaces for Github.com, Gitlab.com, etc. they will all show the contents of the file converted to HTML when someone visits that project. 

-------

## Markdown

'README' explains itself, but the '.md' means that it is a **'Markdown' file**.

[Wikipedia says](https://en.wikipedia.org/wiki/Markdown) "Markdown is a lightweight markup language with plain text formatting syntax designed so that it can be converted to HTML and many other formats using a tool by the same name." This means that you can format text in Markdown's easy formatting rules and it will be converted automatically for you to HTML/etc.

Those familiar with reddit.com's comment/posting system (in addition to many others) will be familiar with Markdown.

-------

## Markdown Cheat Sheet

[Here is a very useful 'cheet sheet'](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) on how to format in Markdown. 

-------